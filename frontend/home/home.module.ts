import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { FormsModule } from '@angular/forms';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    HomeRoutingModule,
    ToastModule,
    TableModule,
    OverlayPanelModule,
    FormsModule,
    DropdownModule
  ],
  declarations: [
    HomeComponent
  ]
})
export class HomeModule { }
