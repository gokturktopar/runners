import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { UserPaceService } from '../services/userpace.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  userPaces : []
  isLoading = false;
  selectedSorted: string = null
  selectedGroup: boolean = false
  constructor(private userPaceService: UserPaceService) { }

  ngOnInit() {
    this.isLoading = true;

}
}
