import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import {Runner} from '../models/runner';
import { RunnerService } from '@app/services/runner.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [MessageService],

})
export class HomeComponent implements OnInit {
  runners : Runner[] = []
  cols : Object[] = []
  isLoading = false;
  sortedBy: string = 'avaragePace'
  sortDirection: string = 'ASC'
  groupBy: boolean = false

  constructor(
    private runnerService: RunnerService,
    private messageService: MessageService,

  ) { }

  ngOnInit() {
    this.cols  = [
      { field: 'userid', header: 'User Id', width: 'auto' },
      { field: 'total_time', header: 'Total Time', sortable:true, width: 'auto'},
      { field: 'distance', header: 'Distance',sortable:true, width: 'auto'},
      { field: 'avaragePace', header: 'Avarage Pace', sortable:true, width: 'auto'},
      { field: 'username', header: 'Username', width: 'auto' },
      { field: 'age', header: 'Age', groupable:true, width: 'auto' },
    ]
    this.getAll();
  }
  getAll() {
    this.isLoading = true
    this.runnerService.getAll(this.sortedBy,this.groupBy, this.sortDirection).pipe(finalize(() => { this.isLoading = false })).subscribe(data => {
      this.runners = data
      console.log(data);
    },
      error => { this.errorMessage('error') })
  }
  group (){
    this.groupBy=!this.groupBy;
    this.getAll()
  }
  sortBy(field: string){
    this.sortedBy = field;
    this.sortDirection = this.sortDirection == 'ASC' ? 'DESC' : 'ASC' ;
    this.getAll()
  }
  errorMessage(detail : any) {
    this.messageService.add(
      { severity: 'error', summary: 'Error Message', detail: detail }
    );
  }
}
