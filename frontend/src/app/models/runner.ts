
export class Runner {
  userid: number;
  total_time: number;
  distance: number;
  avaragePace: number;
  user: {
    userid: number,
    username: string
    age: number
  }

  constructor() {
    this.userid= null;
    this.total_time= null;
    this.distance = null;
    this.avaragePace = null;
    this.user = {
    userid: null,
    username: null,
    age: null
  }
  }
};
