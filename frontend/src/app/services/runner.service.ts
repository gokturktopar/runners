import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpHeaderService } from './http-header.service';

@Injectable({
  providedIn: 'root'
})
export class RunnerService extends HttpHeaderService {
  constructor(
    private http: HttpClient,
    ) { super() }

  public getAll(sortedBy:string, groupBy:boolean, sortDirection:string): Observable<any> {
    const endpoint = `/api/runners?sortedBy=${sortedBy}&sortDirection=${sortDirection}&groupBy=${groupBy}`;
    return this.http.get(endpoint, this.getHeader())
      .pipe(catchError(err => this.handleError(err)));
  }

  // ---------------------------------------------------------------------/
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      if (error.status === 400) {
        if (error.error.validationErrors) {
          error.error.validationErrors.forEach((val: { message: string; }, index: any) => {
          });
        }
      } else if (error.status === 500) {
      }
      else {
        if (!error.error) {
        }
      }
    }
    return throwError('Something bad happed. Please try again.');
  }
}
