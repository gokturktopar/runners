
export class UserPace {
  userid: Number
  total_time: Number
  distance: Number
  avaragePace: Number
  user : {
    userid: Number,
    username: String,
    age: Number,
  }
  constructor() {
    this.user = {
      userid: null,
      username: null,
      age: null,
    },
    this.userid = null
    this.total_time = null
    this.distance = null
    this.avaragePace = null
  }
};
