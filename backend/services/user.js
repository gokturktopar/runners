
const csvParser = require('../utils/parseCsv');
const getAll = async () => {
  const users = await csvParser.parse('users');
  return converter(users)
};
const converter = (data) => {
  return data.map(item => ({...item, userid: parseInt(item.userid),age:parseInt(item.age) }))
}
module.exports = {
    getAll
}
