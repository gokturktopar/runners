
const csvParser = require('../utils/parseCsv');
const getAll = async () =>  {
  const paces = await csvParser.parse('pace');
  return converter(paces)
}

const converter = (data) => {
    return data.map(item => ({userid: parseInt(item.userid), total_time:parseInt(item.total_time),distance:parseInt(item.distance), }))
}
module.exports = {
    getAll
}
