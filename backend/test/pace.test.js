
const chai=require('chai');
const { expect } = require('chai');
const chaiHttp=require('chai-http');
//const testConstants=require('./test.constants');
const server=require('../app');
const should = require('chai').should()
chai.use(chaiHttp);
chai.use(require("chai-sorted"));
const checkBody = (body) => {
  body.forEach(item => {
    expect(item).to.have.property('userid');
    expect(item).to.have.property('total_time');
    expect(item).to.have.property('distance');
    expect(item).to.have.property('avaragePace');
  });
}
  describe('/api/runners',()=>{
    it('should get all pace data with users',(done)=>{
        chai.request(server)
            .get('/api/runners')
            .end((err,res)=>{
                const {body, statusCode} = res
              expect(statusCode).to.equal(200);
              checkBody(body)
              done()

            })
    })
    it('should get all pace data with users sorted by distance',(done)=>{
      chai.request(server)
          .get('/api/runners?sortedBy=distance')
          .end((err,res)=>{
            const {body, statusCode} = res
            expect(statusCode).to.equal(200);
            checkBody(body)
            expect([body]).to.be.sortedBy("distance")
            done()

      })
    })
    it('should get all pace data with users sorted by total time',(done)=>{
      chai.request(server)
          .get('/api/runners?sortedBy=total_time')
          .end((err,res)=>{
            const {body, statusCode} = res
            expect(statusCode).to.equal(200);
            expect([body]).to.be.sortedBy("total_time")
            checkBody(body)
            done()

      })
    })
    it('should get all pace data with users sorted by avarage pace',(done)=>{
      chai.request(server)
          .get('/api/runners?sortedBy=avaragePace')
          .end((err,res)=>{
            const {body, statusCode} = res
            expect(statusCode).to.equal(200);
            expect([body]).to.be.sortedBy("avaragePace")
            checkBody(body)

              done()
      })
    })
  });
