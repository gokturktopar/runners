const express = require('express');
const router = express.Router();
const paceController = require('../controllers/pace'); 

router.get('/', async (req, res, next) => {
  const paces = await paceController.getAll(req.query);
  res.json(paces);
});

module.exports = router;
