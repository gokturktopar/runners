
const { rejects } = require('assert');
const csv = require('csv-parser')
const fs = require('fs');
 
module.exports.parse = (fileName) => {
    return new Promise((resolve,reject) => {
    const results = [];
        try {
            fs.createReadStream(`./data/${fileName}.csv`)
            .pipe(csv())
            .on('data', (data) => results.push(data))
            .on('end', () => {
                resolve(results);
            });  
        } catch (error) {
            reject(error);
        }
    })
     
}