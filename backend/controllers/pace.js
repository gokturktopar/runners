const userService = require('../services/user')
const paceService = require('../services/pace')

const getAll = async (queryString) => {
  const paces = await paceService.getAll();
    const users = await userService.getAll();
    const {sortedBy, sortDirection, groupBy } = extractQuery(queryString);
   const mathcedItems = paces.map(pace => {
      const user = users.find( ({userid}) => userid === pace.userid)
      if (user && groupBy) {
        user.group = getUserGroup(user.age);
      }
      const avaragePace = parseInt((pace.distance / pace.total_time).toFixed(2));
      return {...pace, avaragePace, user};
    });
    const sortedData = sortBy(mathcedItems, sortedBy, sortDirection);
    return sortedData;
}

const sortBy = (data, field, direction) =>  data.sort((s1, s2) => {
  if (direction === 'ASC') {
    return s1[field] - s2[field];
  }
  else{
    return s2[field] - s1[field];
  }
});

const getUserGroup = (age) => {
  if(age >= 20 && age <= 30)
    return 1
  if(age > 30 && age <= 40)
    return 1
  if(age > 40 && age <= 60)
    return 3
}

const extractQuery = (query) => {
  const sortedBy = query.sortedBy || 'avaragePace';
  const sortDirection = query.sortDirection || 'ASC';
  const groupBy = query.groupBy === 'true' || false; // states data will be grouped or not
  return {groupBy, sortedBy, sortDirection};
}

module.exports = {
    getAll,
    sortBy
}
